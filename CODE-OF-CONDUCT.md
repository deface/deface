
# Deface Code of Conduct

We believe that our mission is best served in an environment that is friendly, safe, and accepting; free from intimidation or harassment.

Towards this end, certain behaviors and practices will not be tolerated.


## tl;dr

- Be respectful.
- We're here to help: [admin@deface.app](mailto:admin@deface.app)
- Abusive behavior is never tolerated.
- Violations of this code may result in swift and permanent expulsion from the Deface community.
- "Too long, didn't read" is not a valid excuse for not knowing what is in this document.


## Scope

We expect all members of the Deface community to abide by this Code of Conduct at all times in all Deface community venues, online and in person, and in one-on-one communications pertaining to Deface affairs.

This policy covers the usage of all Deface websites, Deface related events, and any other services offered by or on behalf of the Deface community. It also applies to behavior in the context of the Deface Open Source project communities, including but not limited to public Gitlab repositories, social media, mailing lists, and public events.

This Code of Conduct is in addition to, and does not in any way nullify or invalidate, any other terms or conditions related to use of the Service.

The definitions of various subjective terms such as "discriminatory", "hateful", or "confusing" will be decided at the sole discretion of the Deface abuse team.


## Friendly Harassment-Free Space

We are committed to providing a friendly, safe and welcoming environment for all, regardless of gender identity, sexual orientation, ability, ethnicity, religion, age, physical appearance, body size, race, or similar personal characteristics.

We ask that you please respect that people have differences of opinion regarding technical choices, and that every design or implementation choice carries a trade-off and numerous costs. There is seldom a single right answer. A difference of technology preferences is not a license to be rude.

Any spamming, trolling, flaming, baiting, or other attention-stealing behavior is not welcome, and will not be tolerated.

Harassing other users of the Service is never tolerated, whether via public or private media.

Avoid using offensive or harassing package names, nicknames, or other identifiers that might detract from a friendly, safe, and welcoming environment for all.

Harassment includes, but is not limited to: harmful or prejudicial verbal or written comments related to gender identity, sexual orientation, ability, ethnicity, religion, age, physical appearance, body size, race, or similar personal characteristics; inappropriate use of nudity, sexual images, and/or sexually explicit language in public spaces; threats of physical or non-physical harm; deliberate intimidation, stalking or following; harassing photography or recording; sustained disruption of talks or other events; inappropriate physical contact; and unwelcome sexual attention.


## Reporting Violations of this Code of Conduct

If you believe someone is harassing you or is demonstrating some other form of malicious or inappropriate behavior, contact [admin@deface.app](mailto:admin@deface.app). If this is the initial report of a problem, please include as much detail as possible. It is easiest for us to address issues when we have more context.

## Consequences

Unacceptable behavior from any community member will not be tolerated.

Anyone asked to stop unacceptable behavior is expected to comply immediately.

If a community member engages in unacceptable behavior, the Deface team may take any action they deem appropriate, up to and including a temporary ban or permanent expulsion from the community without warning.


## Addressing Grievances

If you feel you have been falsely or unfairly accused of violating this Code of Conduct, you should notify the Deface team. We will do our best to ensure that your grievance is handled appropriately.

In general, we will choose the course of action that we judge as being most in the interest of fostering a safe and friendly community.


## Contact Info

Please contact [admin@deface.app](mailto:admin@deface.app) if you need to report a problem or address a grievance related to an abuse report.

You are also encouraged to contact us if you are curious about something that might be "on the line" between appropriate and inappropriate content. We are happy to provide guidance to help you be a successful part of our community.


## Changes

This is a living document and may be updated from time to time. Please refer to the [git history](https://gitlab.com/deface/deface/blob/master/CODE-OF-CONDUCT.md) for this document to view the changes.


## Credit and License

This Code of Conduct borrows heavily from the Stumptown Syndicate [Citizen's Code of Conduct](http://citizencodeofconduct.org/), and the [Rust Project Code of Conduct](https://www.rust-lang.org/conduct.html).

This document may be reused under a [Creative Commons Attribution-ShareAlike License](https://creativecommons.org/licenses/by-sa/4.0/).
